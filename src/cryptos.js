
/**
 * A class to do useless things related to cryptos...
 */
class Cryptos {
  /**
   * Provide a list of cryptocurrencies
   * @return {Array}   A list of crypto currencies
   */
  list () {
    return ['Bitcoin', 'Ethereum']
  }
}

module.exports = {
  Cryptos
}
